<?php if ($env == 'dev'): ?>
	
<div class='footer section' id='footer1'><div class='widget ContactForm' data-version='1' id='ContactForm1'>
<h2 class='title'>Formularz kontaktowy</h2>
<div class=''>
<div class='form'>
<form name='contact-form-custom'>
<div class='alerts'></div>
<div class='row row-half-gutter'>
	<div class='col-sm-6 col-lg-12'>
		<div class='form-group'>
		<input class='form-control' id='ContactForm1_contact-form-name' name='name' placeholder='Nazwa' type='text'/>
		</div>
	</div>
	<div class='col-sm-6 col-lg-12'>
		<div class='form-group'>
		<input class='form-control' id='ContactForm1_contact-form-email' name='email' placeholder='E-mail *' required='required' type='email' style=''/>
		</div>
	</div>
</div>
<div class='form-group'>
<textarea class='form-control' id='ContactForm1_contact-form-email-message' name='message' required='required' placeholder='Wiadomość *' rows='6' value='Wyślij' style='resize: none'></textarea>
</div>
<div class='form-group xxs-mb-0'>
<button class='btn btn-default' id='ContactForm1_contact-form-submit'>Wyślij</button>
</div>
</form>
</div>
</div>
<div class='clear'></div>
</div></div>

<?php else: ?>

<b:widget id='HTML1' locked='false' title='Formularz kontaktowy' type='HTML'>
  <b:widget-settings>
    <b:widget-setting name='content'></b:widget-setting>
  </b:widget-settings>
  <b:includable id='main'>
  <!-- only display title if it's non-empty -->
  <b:if cond='data:title != &quot;&quot;'>
    <h2 class='title'><data:title/></h2>
  </b:if>
	<div class='widget-content'>
		<form name='contact-form-custom'>
			<div class='alerts'></div>
			
			<div class='form-group'>
				<input class='form-control' id='ContactForm1_contact-form-name' name='name' placeholder='Nazwa' type='text'/>
			</div>
			<div class='form-group'>
				<input class='form-control' id='ContactForm1_contact-form-email' name='email' placeholder='E-mail *' required='required' type='email'/>
			</div>
			<div class='form-group'>
				<textarea class='form-control' id='ContactForm1_contact-form-email-message' name='message' required='required' placeholder='Wiadomość *' rows='6' value='Wyślij' style='resize: none'></textarea>
			</div>
			<div class='form-group xxs-mb-0'>
				<button class='btn btn-default' id='ContactForm1_contact-form-submit'>Wyślij</button>
			</div>
		</form>
	</div>

  <b:include name='quickedit'/>
</b:includable>
</b:widget>

<?php endif ?>