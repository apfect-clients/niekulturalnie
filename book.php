<?php /*<img style='width: 200px;' src="<?php include 'logo-base64.php' ?>">
<br>
<br> */ ?>

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   version="1.1"
   id="book"
   viewBox="0 0 469.35686 233.16671"
   height="99.34"
   width="200">
  <defs
     id="defs4" />
  <metadata
     id="metadata7">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     transform="translate(-116.39298,-312.05422)"
     id="layer1">
    <path
       id="path4144"
       d="m 383.10035,532.89126 c 31.62853,-16.09309 79.74376,2.08115 152.59366,5.51752 13.29608,0.62719 23.35505,0.998 36.80994,1.3903 -11.35168,-3.96427 -25.56836,-17.61857 -35.14921,-22.37393 -26.32529,-3.16236 -25.19819,-3.74646 -48.70756,-8.42496 -95.97066,-19.09874 -138.97155,-2.23486 -139.1344,24.64868 0.24365,-31.1574 -39.21823,-44.20463 -136.61579,-25.06799 -28.64521,5.6282 -21.99702,4.51517 -45.54779,6.8673 -7.69653,6.79353 -22.88617,17.64647 -39.61655,23.74952 18.06759,-0.49726 11.69167,-0.24903 33.15553,-1.24178 84.11142,-3.89034 128.40968,-21.46168 159.2039,-4.68585 24.05404,10.45388 44.34155,8.05285 63.00827,-0.37881 z"
       style="fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
    <path
       id="path4142"
       d="m 118.92857,542.00506 199.64286,-6.78571 c 24.06722,8.87429 42.46419,8.83176 65,0 l 199.64286,7.5"
       style="fill:none;fill-rule:evenodd;stroke:#ffffff;stroke-width:5;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
    <path
       id="book-page"
       d="m 349.72199,535.76897 c 1.47457,-43.0782 -62.40561,-36.12582 -182.37279,-20.32079"
       style="fill:none;fill-rule:evenodd;stroke:#ffffff;stroke-width:2.4px;stroke-linecap:square;stroke-linejoin:miter;stroke-opacity:1" />
  </g>
</svg>

<script type="text/javascript" src="<?=$assetsBase?>/js/book.js?<?=time()?>"></script>