<?php

session_start();

$nav = [
	['title'=>"<i class='fa fa-home'></i>"],
	['title'=>'Podmenu', 'items'=>[
		['title'=>'Podmenu 1'],
		['title'=>'Podmenu 2'],
		['title'=>'Podmenu 3 - bardzo długi tekst i DUŻY TEKST'],
	]],
	['title'=>'Współpraca'],
	['title'=>'Recenzje'],
	['title'=>'Okołoksiążkowe'],
	['title'=>'Moje obrazki'],
	['title'=>'Kontakt']
];