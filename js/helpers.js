import velocity from 'velocity-animate';
import { cache } from '../js/data-processing.js';

$.fn.hook = function(property, value)
{
	velocity.hook(this, property, value);
    return this;
};

if (history)
{
    history.changeState = function(object)
    {
        history.replaceState(Object.assign(history.state ? history.state : {}, object), null, window.location.href);
    };
    history.set = function(name, value)
    {
        var object = {};
        object[name] = value;
        history.changeState(object);
    };
    history.get = function(name, defaultValue = null)
    {
        if (history.has(name)) return history.state[name];
        return defaultValue;
    };
    history.has = function(name)
    {
        return history.state && history.state.hasOwnProperty(name) && history.state[name] != null;
    };
}

export function getHistoryScrollTop()
{
    return history.state && history.state.scrollTop ? history.state.scrollTop : 0;
}

export function getHistoryOrHashScrollTop()
{
    if (window.location.hash && $(window.location.hash).length)
    {
        return $(window.location.hash).offset().top - $('.content-container').offset().top - 50;
    }
    return getHistoryScrollTop();
}

export function restoreScrollTop(force = false)
{
    if (window.loaded && (force || !window.scrolled))
        $('.scroll-container').add(window).scrollTop(getHistoryOrHashScrollTop());
}

window.restoreScrollTop = restoreScrollTop;

export function preloadImage(imageUrl, callback = null, timeout = null)
{
    var img = new Image();
    img.onload = function()
    {
        if (callback) callback(), callback = null;
    };
    img.src = imageUrl;
    if (timeout !== null && callback)
        setTimeout(() => (callback(), callback = null), timeout);
}

export function preloadImages(images, callback = null, timeout = null)
{
    let imagesLeft = images.length;
    if (imagesLeft == 0)
    {
        if (callback) callback();
        return;
    }
    images.forEach(imageUrl => preloadImage(imageUrl, function()
    {
        imagesLeft--;
        if (callback && imagesLeft == 0) callback(), callback = null;
    }));
    if (timeout !== null)
        setTimeout(() => (!callback || callback(), callback = null), timeout);
}

export function setMeta(meta = {})
{
    var defaultMeta =
    {
        title: cache.title,
        description: cache.description,
        'og:url': window.location.href,
        'og:type': 'website',
        'og:image': $('base').attr('href')+'public/img/logo.png',
    };
    if (meta.pageTitle)
    {
        meta.title = `${meta.pageTitle} | ${cache.title}`;
        delete meta.pageTitle;
    }
    meta = Object.assign(defaultMeta, meta);
    meta['og:title'] = meta.title;
    meta['og:description'] = meta.description;
    for (let [key, value] of Object.entries(meta))
    {
        var nameOrProperty = key.startsWith('og:') ? 'property' : 'name';
        var $meta = $(`meta[${nameOrProperty}='${key}']`);
        if (!$meta.length) $meta = $(`<meta ${nameOrProperty}='${key}'>`).appendTo('head');
        $meta.attr('content', value);
    }
    document.title = meta.title;
}