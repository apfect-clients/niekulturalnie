import { cachePost } from './data-processing.js'

export var dataProcessingWorker = new Worker($('body').data('assets')+'/js/data-processing-worker.js');

dataProcessingWorker.listeners = [];

dataProcessingWorker.onmessage = function(event)
{
	dataProcessingWorker.listeners[event.data.id](event.data.response);
};

dataProcessingWorker.call = function(name, params, callback)
{
	dataProcessingWorker.listeners.push(callback);
	dataProcessingWorker.postMessage(
	{
		name: name,
		params: params,
		id: dataProcessingWorker.listeners.length - 1
	});
};

export function preparePostsFromFeedAsync(feed, callback)
{
	dataProcessingWorker.call('preparePostsFromFeed', [feed], function(posts)
	{
		for (var post of posts) cachePost(post);
		callback(posts);
	});
}