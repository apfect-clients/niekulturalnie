import 'snapsvg-cjs';

//****************************************************************************************************//
// animacja książki

var s = Snap('#book');
    
var page = s.select('#book-page');

var keyframes = [
	['m 349.72199,535.76897 c 1.47457,-43.0782 -62.40561,-36.12582 -182.37279,-20.32079', null],
	['m 349.72198,535.76897 c 1.47457,-43.0782 -57.67087,-62.42059 -181.17412,-35.65311', null],
	['M 349.72175,535.76881 C 351.19632,492.6906 300.66783,438.33486 175.736,457.95949', null],
	['M 349.72212,535.76881 C 351.19669,492.6906 319.70525,425.1261 193.34485,413.32216', null],
	['M 349.72198,535.76897 C 351.19655,492.69076 337.61274,410.23662 225.89956,368.12811', null],
	['m 349.72198,535.76897 c 1.47457,-43.07821 -1.67682,-139.95895 -76.01436,-203.78574', null],
	['m 349.72198,535.76897 c 1.47457,-43.07821 2.72617,-136.55895 -28.17481,-220.58879', null],
	['m 349.72198,535.76897 c 1.47457,-43.07821 5.07178,-126.53707 15.59488,-223.20767', null],
	['m 349.72198,535.76897 c 1.47457,-43.07821 12.8023,-117.43272 56.66043,-215.11347', null],
	['M 349.72198,535.76897 C 351.19655,492.69076 379.64033,425.26674 450.2675,342.2332', null],
	['m 349.72198,535.76897 c 1.77111,-43.56925 46.36366,-99.3646 139.71926,-158.15448', null],
	['m 349.72198,535.76897 c 1.77111,-43.56925 66.46906,-96.78289 167.74033,-114.55426', null],
	['m 349.72198,535.76897 c 1.77111,-43.56925 71.9291,-82.68641 181.96309,-75.57379', 120],
	['m 349.72198,535.76897 c 1.77111,-43.56925 66.61441,-63.19316 188.1166,-40.83011', 150],
	['m 349.72196,535.76898 c 1.77111,-43.56926 67.54589,-34.94828 187.63278,-18.34383', 200],
];

var booki = 1;
var nextPageTimeout = null;

function animate()
{
	if (booki >= keyframes.length)
	{
		resetBookAnimation();
		nextPageTimeout = setTimeout(() => startBookAnimation(), 1000);
	}
	else page.animate({ path: keyframes[booki][0] }, keyframes[booki++][1] || 100, mina.linear, animate);
}

function resetBookAnimation()
{
	page.stop();
	if (nextPageTimeout) clearTimeout(nextPageTimeout);
	booki = 1;
	page.attr({ path: keyframes[0], opacity: 0 });
}

function startBookAnimation()
{
	page.attr({ opacity: 1 });
	animate();
}

startBookAnimation();

window.startBookAnimation = startBookAnimation;
window.resetBookAnimation = resetBookAnimation;

if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

document.getElementById('temp-curtain').remove();

//****************************************************************************************************//