import VueRouter from 'vue-router';
import Vue from 'vue'
import HomePage from '../pages/HomePage.vue';
import PostPage from '../pages/PostPage.vue';
import LabelPage from '../pages/LabelPage.vue';
import SearchPage from '../pages/SearchPage.vue';
import ArchivePage from '../pages/ArchivePage.vue';
import ReviewsPage from '../pages/ReviewsPage.vue';
import ErrorPage from '../pages/ErrorPage.vue';
import TextPage from '../pages/TextPage.vue';
import PreviewPage from '../pages/PreviewPage.vue';

Vue.use(VueRouter);

var router = new VueRouter(
{
    mode: 'history',
    routes:
    [
        { path: '/', component: HomePage, name: 'home' },
        { path: '/:year/:month/:url.html', component: PostPage, name: 'post' },
        { path: '/p/:url.html', component: TextPage, name: 'page' },
        { path: '/search/label/recenzja', component: ReviewsPage },
        { path: '/search/label/:label', component: LabelPage },
        { path: '/search', component: SearchPage },
        { path: '/b/post-preview', component: PreviewPage },
        { path: '/:year([0-9]+)/:month([0-9]+)?', component: ArchivePage },
        { path: '*', component: ErrorPage },
    ],
});

router.afterEach(function(to, from)
{
    window.scrolled = false;
    if ($('.navbar-search-box').length) $('.btn-search-toggle')[0].dispatchEvent(new Event("click"));
    setTimeout(function()
    {
        if ($('.menu-fullscreen').length) $('.btn-fullscreen-menu-toggle')[0].dispatchEvent(new Event("click"));
    });
    $('.tooltip').hide();
    restoreScrollTop();
});

router.pageNotFound = function()
{
    var url = window.location.href;
    this.replace('/error');
    history.replaceState(history.state, null, url);
};

export default router;