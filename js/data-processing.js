import moment from 'moment';

export var cache = {
	posts: {},
	postsByPath: {},
	refreshRate: 0 // w minutach
};

export function isRefreshTime(time)
{
	return moment().diff(time, 'minutes') >= cache.refreshRate;
}

export function shuffleArray(array)
{
    for (let i = array.length - 1; i > 0; i--)
    {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
} 

export function prepareSettings(content)
{
	var $content = $('<div>').html(content);
	function readSettingsUl($ul, settings = {})
	{
		var key = null;
		$ul.children().each(function()
		{
			if ($(this).is('li'))
			{
				var text = $(this).text().split(':');
				key = text.shift().trim();
				var val = text.join(':').trim();
				var item = { key: key, value: val == '' ? null : val, text: $(this).text().trim() };
				var keyIndex = 2;
				if (key in settings)
				{
					while (`${key} (${keyIndex})` in settings) key++;
					settings[`${key} (${keyIndex})`] = item;
				}
				else settings[key] = item;
			}
			else if ($(this).is('ul'))
				settings[key].items = readSettingsUl($(this), settings[key].items || {});
		});
		return settings;
	};
	return readSettingsUl($content.find('ul').first());
}

export function cachePost(post)
{
	if (cache.posts[post.id])
	{
		post.comments = cache.posts[post.id].comments;
		post.relatedPosts = cache.posts[post.id].relatedPosts;
	}
	cache.posts[post.id] = post;
	cache.postsByPath[post.path] = post;
}

export var htmlDocument = document.implementation.createHTMLDocument('temp');

function preparePostContent(post)
{
	var $content = post.$content;
	$content.find('img').each(function()
	{
		$(this).attr('data-arise', 'fadeIn2').css('opacity', 0);
		if ($(this).css('float') == 'left' || $(this).css('float') == 'right')
			$(this).addClass('img-'+$(this).css('float'));
		else if ($(this).parent().css('float') == 'left' || $(this).parent().css('float') == 'right')
			$(this).addClass('img-'+$(this).parent().css('float'));
		$(this).parent().css('margin-left', '');
		$(this).parent().css('margin-right', '');
		if ($(this).is('[src*="/s640/"]'))
		{
			$(this).addClass('img-xl');
			$(this).attr('src', $(this).attr('src').replace('/s640/', '/s690/'));
		}
	});
	$content.find('.YOUTUBE-iframe-video').wrap('<div class="youtube-iframe-container" data-arise="fadeIn2"></div>');
	post.content = $content.html();
	if (post.hasMainPhoto)
	{
		var $img = $content.find('img[src*="blogspot.com"]:first');
		var $child = $img;
		while ($child.is(':empty'))
		{
			var $parent = $child.parent();
			$child.remove();
			$child = $parent;
		}
	}
	post.contentWithoutPhoto = $content.html();
}

export function preparePost(post)
{
	var contentElement = htmlDocument.createElement('div');
	contentElement.innerHTML = post.content;
	var $content = $(contentElement);
	post.rawContent = post.content;
	post.$content = $content;
	if ($content.find('img[src*="blogspot.com"]:first').length)
	{
		var $img = $content.find('img[src*="blogspot.com"]:first');
		var photo = $img.attr('src').split('/');
		photo[photo.length - 2] = 's960';
		post.photo = photo.join('/');
		photo[photo.length - 2] = 's1600';
		post.photoFull = photo.join('/');
		post.photoCustom = function(format)
		{
			if (Number.isInteger(format)) format = 's'+format;
			photo[photo.length - 2] = format;
			return photo.join('/');
		};
	}
	else
	{
		post.photo = post.photoFull = 'public/img/logo.png';
		post.photoCustom = format => 'public/img/logo.png';
	}
	post.path = $('<a>').attr('href', post.url)[0].pathname;
	if (env == 'staging') post.url = post.url.replace('//www.niekulturalnie.pl', '//niekulturalnie-test.blogspot.com');
	else if (env == 'dev') post.url = post.url.replace('//www.niekulturalnie.pl', '//'+window.location.hostname+'/blogkasi');
	var time = moment(post.published);
	post.archiveUrl = `${time.year()}/${time.format('MM')}`;
	post.when = time.format('dddd, D MMMM YYYY');
	var introElement = htmlDocument.createElement('div');
	var $intro = $(introElement).html(!post.content.includes("<a name='more'></a>") ? '' : post.content.split("<a name='more'></a>")[0]);
	post.hasMainPhoto = $intro.find('img[src*="blogspot.com"]').length;
	post.intro = $intro.text();
	post.prepareContent = function()
	{
		if (!post.contentWithoutPhoto) preparePostContent(post); 
	};
	if (!post.labels) post.labels = [];
	post.comments = post.relatedPosts = null;
	cachePost(post);
	return post;
}

export function preparePosts(posts)
{
	for (var post of posts) preparePost(post);
	return posts;
}

export function preparePostFromFeed(entry)
{
	var post = entry;
	var postId = post.id.$t.split('-').slice(-1)[0];
	var postUpdated = post.updated.$t;
	if (cache.posts[postId] && cache.posts[postId].updated == postUpdated) return cache.posts[postId];
	post = {
		title: post.title.$t,
		content: post.content.$t,
		published: post.published.$t,
		id: postId,
		updated: postUpdated,
		url: post.link.find(e=>e.rel == "alternate").href,
		author: {
			displayName: post.author[0].name.$t,
			url: post.author[0].uri.$t,
		},
		labels: post.category ? post.category.map(e=>e.term) : [],
		blog: { id: post.id.$t.split('-')[1].split('.')[0] }
	};
	return preparePost(post);
}

export function preparePostsFromFeed(feed)
{
	var posts = [];
	if (feed.entry)
		for (var entry of feed.entry)
			posts.push(preparePostFromFeed(entry));
	return posts;
}

export function preparePostLinksFromFeed(feed)
{
	var posts = [];
	if (feed.entry)
		for (var entry of feed.entry)
		{
			var post =
			{
				id: entry.id.$t.split('-').slice(-1)[0],
				title: entry.title.$t,
				url: entry.link.find(e=>e.rel == "alternate").href,
			};
			if (env == 'staging') post.url = post.url.replace('//www.niekulturalnie.pl', '//niekulturalnie-test.blogspot.com');
			else if (env == 'dev') post.url = post.url.replace('//www.niekulturalnie.pl', '//'+window.location.hostname+'/blogkasi');
			posts.push(post);
		}
	return posts;
}

export function preparePostFromWidget(post)
{
	post = {
		title: post.title,
		content: post.body,
		published: moment(post.timestamp, 'M/D/YYYY hh:mm:ss A'),
		id: post.id,
		updated: moment(post.timestamp, 'M/D/YYYY hh:mm:ss A'),
		url: post.url,
		author: {
			displayName: post.author,
			url: post.authorProfileUrl,
		},
		labels: post.labels ? post.labels.map(e=>e.name) : [],
		//blog: { id: post.id.$t.split('-')[1].split('.')[0] }
	};
	return preparePost(post);
}

export function prepareCommentFromFeed(entry)
{
	var comment = entry;
	comment.content = comment.content.$t;
	comment.published = comment.published.$t;
	comment.id = comment.id.$t.split('-').pop();
	comment.author = {
		url: comment.author[0].uri ? comment.author[0].uri.$t : null,
		displayName: comment.author[0].name.$t,
		image: { url: comment.author[0].gd$image.src },
		owner: comment.author[0].uri && comment.author[0].uri.$t == 'https://www.blogger.com/profile/03755360839444999556',
	};
	if (comment.author.image.url.indexOf('blogspot.com') != -1)
	{
		var image = comment.author.image.url.split('/');
		image[image.length - 2] = 's70-c';
		comment.author.image.url = image.join('/');
	}
	comment.itemClass = comment.gd$extendedProperty.find(x => x.name == 'blogger.itemClass').value;
	if (comment.link.find(x => x.rel == 'related')) comment.inReplyTo = { id: comment.link.find(x => x.rel == 'related').href.split('/').pop() };
	comment.content = comment.content.replace(/(?:<br[^>]*>\s*){3,}/g, '<br><br>');
	comment.showForm = false;
	var time = moment(comment.published);
	comment.date = time.format('ddd, D MMMM YYYY o HH:mm');
	comment.when = time.fromNow();
	/*if (moment().diff(time.clone().startOf('day'), 'years') >= 1)
		comment.when = time.format('D MMMM YYYY o HH:mm');
	else if (moment().diff(time.clone().startOf('day'), 'weeks') >= 1)
		comment.when = time.format('D MMMM o HH:mm');
	else if (moment().diff(time, 'minutes'))
		comment.when = time.calendar();
	else
		comment.when = time.fromNow();*/
	return comment;
}

export function prepareCommentsFromFeed(feed)
{
	if (!feed.entry) return [];
	var comments = [];
	for (var entry of feed.entry) comments.push(prepareCommentFromFeed(entry));
	for (var comment of comments)
	{
		if (comment.inReplyTo)
		{
			var parent = comments.find(c => c.id == comment.inReplyTo.id);
			if (!parent.items) parent.items = [];
			parent.items.push(comment);
		}
	}
	comments = comments.filter(c => !c.inReplyTo);
	return comments;
}

export function prepareCommentFromWidget(comment)
{
	comment.content = comment.body;
	comment.author = {
		url: comment.author.profileUrl,
		displayName: comment.author.name,
		image: { url: comment.author.avatarUrl },
		owner: comment.author.profileUrl && comment.author.profileUrl == 'https://www.blogger.com/profile/03755360839444999556',
	};
	if (comment.author.image.url.indexOf('blogspot.com') != -1)
	{
		var image = comment.author.image.url.split('/');
		image[image.length - 2] = 's70-c';
		comment.author.image.url = image.join('/');
	}
	comment.itemClass = comment.deleteclass;
	if (comment.parentId) comment.inReplyTo = { id: comment.parentId };
	comment.content = comment.content.replace(/(?:<br[^>]*>\s*){3,}/g, '<br><br>');
	comment.showForm = false;
	var time = moment.unix(comment.timestamp/1000);
	comment.date = time.format('ddd, D MMMM YYYY o HH:mm');
	comment.when = time.fromNow();
	return comment;
}

export function prepareCommentsFromWidget(comments)
{
	for (var comment of comments)
	{
		prepareCommentFromWidget(comment);
		if (comment.parentId)
		{
			var parent = comments.find(c => c.id == comment.parentId);
			if (!parent.items) parent.items = [];
			parent.items.push(comment);
		}
	}
	comments = comments.filter(c => !c.parentId);
	return comments;
}
