import * as processing from './data-processing.js';

onmessage = function(event)
{
	var response = processing[event.data.name].apply(null, event.data.params);
	postMessage(
	{
		id: event.data.id,
		response: response
	});
};