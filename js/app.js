import 'core-js';
import 'url-search-params-polyfill';
import $ from 'jquery';
import 'jquery-migrate';
import 'bootstrap';
import objectFitImages from 'object-fit-images';
//import 'owl.carousel';
import Swiper from 'swiper';
import 'velocity-animate';
import 'jquery-mousewheel';
import ResizeSensor from 'css-element-queries/src/ResizeSensor';
import 'jquery.easing';
import 'magnific-popup';
import Vue from 'vue';
import moment from 'moment';
import imagesLoaded from 'imagesloaded';

import { restoreScrollTop } from './helpers.js';
import './request-animation-frame.js';
import { prepareSettings, preparePostsFromFeed, cache } from './data-processing.js';
//import { preparePostsFromFeedAsync } from './data-processing-async.js';
import router from './router.js';
import AppComponent from '../components/AppComponent.vue';

window.cache = cache;

moment.locale('pl');

//****************************************************************************************************//
// wykrywanie środowiska

window.mobileAndTabletCheck = function()
{
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

var mobileDevice = window.mobileAndTabletCheck();
var isIE = navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1);
var isEdge = /edge/i.test(navigator.userAgent);
var isIEorEdge = isIE || isEdge;
var isFirefox = /firefox/i.test(navigator.userAgent);
$('html').toggleClass('mobile-device', mobileDevice);
$('html').toggleClass('not-mobile-device', !mobileDevice);

//****************************************************************************************************//
// zmienne globalne

var env = window.env = $('body').data('env');
var pageType = $('body').data('pagetype');

var blogId = env == 'staging' ? '5645147778766638791' : '1483976098770485391';
var apiBase = `https://www.googleapis.com/blogger/v3/blogs/${blogId}/`;
var feedBase = window.feedBase = env == 'staging' ? 'http://niekulturalnie-test.blogspot.com/feeds/' : 'http://www.niekulturalnie.pl/feeds/';
var settingsPageId = env == 'staging' ? '3431069924174797466' : '1357416388490248332';
var apiKey = 'AIzaSyDNstSVBIBsMY7UtGQKPdPWwgWOgcuFYXE';
var domain = env == 'dev' ? 'localhost' : env == 'staging' ? 'niekulturalnie-test.blogspot.com' : 'niekulturalnie.pl';

var params = $('.params').data();
params = { view: 'post' };

var loadConditions = 1;
var vueView = null;
var settings = null;

window.pushLoadCondition = (conditions = 1) => loadConditions += conditions;
window.popLoadCondition = () => !--loadConditions ? pageLoaded() : null;

//****************************************************************************************************//
// wstawianie sekcji bloggera

function insertBloggerSections()
{
	$('.blogger-insert-section').each(function()
	{
		$(this).replaceWith($(`[id="${$(this).data('id')}"]`));
	});
	$('.blogger-sections').hide();
}

//****************************************************************************************************//
// ładowanie wszystkich stron oraz ustawień

$.getJSON(feedBase+'pages/default?callback=?', { alt: 'json', 'max-results': 999 }, function(feed)
{
	preparePostsFromFeed(feed.feed);
	cache.title = feed.feed.title.$t;
	cache.description = feed.feed.subtitle.$t;
	cache.settings = window.settings = prepareSettings(cache.posts[settingsPageId].content);
	setTimeout(function() // w celu upłynnienia animacji książki
	{
		vueView = initView();
		if (!--loadConditions) pageLoaded();
	}, 50);
	/*preparePostsFromFeedAsync(feed.feed, function(data)
	{
		cache.title = feed.feed.title.$t;
		cache.description = feed.feed.subtitle.$t;
		cache.settings = window.settings = prepareSettings(cache.posts[settingsPageId].content);
		vueView = initView();
		if (!--loadConditions) pageLoaded();
	});*/
});

//****************************************************************************************************//
// generowanie widoku

function initView()
{
	var app = new Vue(
	{
		router,
		render: h => h(AppComponent)
	})
	.$mount('#app');

	return app;
}

//****************************************************************************************************//
// slider

function initSlider()
{
	var $slider = $('.slider');
	if (!$slider.length || $slider.data('initialized')) return;
	$slider.data('initialized', true);

	var swiper = new Swiper($slider,
	{
		loop: true,
		speed: 800,
		autoplay: { delay: 5000, disableOnInteraction: false },
		navigation:
		{
			nextEl: '.slider-next',
			prevEl: '.slider-prev',
		},
		slidesPerView: 2,
		breakpoints:
		{
			991: { slidesPerView: 1 }
		}
	});

	window.swiper = swiper;

	swiper.autoplay.stop();

	swiper.on('transitionStart', function()
	{
		var $toArise = $();
		for (var i = swiper.activeIndex; i < swiper.activeIndex + swiper.params.slidesPerView; i++)
		{
			var slideId = $(swiper.slides[i]).data('id');
			$toArise = $toArise.add('.slide[data-id="'+slideId+'"]', $slider);
		}
		$toArise.each(function() { ariseSlide($(this)); });
		$slider.find('.slide.arised').not($toArise).each(function() { resetSlide($(this)); });
	});

	$slider.find('.slide[data-swiper-slide-index=0] .slide-container').hook('translateX', '-100%');
	$slider.find('.slide[data-swiper-slide-index=1] .slide-container').hook('translateX', '100%');
	$slider.find('.slide').each(function() { resetSlide($(this), false); });

	function resetSlide($slide, animation = true)
	{
		$slide.removeClass('arised');
		var $elements = $slide.find('.slide-labels, .slide-title, .slide-date-and-author');
		if (animation)
			$elements.velocity('stop').velocity({ translateY: 20, opacity: 0 }, 600);
		else
			$elements.hook('translateY', '20px').hook('opacity', '0');
	}

	function ariseSlide($slide)
	{
		if ($slide.hasClass('arised')) return;
		$slide.find('.slide-labels, .slide-title, .slide-date-and-author').velocity('stop');
		$slide.find('.slide-labels').delay(300).velocity({ translateY: 0, opacity: 1 }, 800, 'easeOutCubic');
		$slide.find('.slide-title').delay(400).velocity({ translateY: 0, opacity: 1 }, 800, 'easeOutCubic');
		$slide.find('.slide-date-and-author').delay(500).velocity({ translateY: 0, opacity: 1 }, 800, 'easeOutCubic');
		$slide.addClass('arised');
	}

	window.ariseSlider = function()
	{
		var duration = 600;
		$slider.find('.slide[data-swiper-slide-index=0] .slide-container').velocity({ translateX: 0 }, duration, 'easeOutQuart');
		$slider.find('.slide[data-swiper-slide-index=1] .slide-container').velocity({ translateX: 0 }, duration, 'easeOutQuart');
		swiper.autoplay.start();
		$slider.find('.slide[data-swiper-slide-index=0]').each(function() { ariseSlide($(this)); });
		if (swiper.params.slidesPerView == 2) $slider.find('.slide[data-swiper-slide-index=1]').each(function() { ariseSlide($(this)); });
	}
}

//****************************************************************************************************//
// płynne przewijanie

var scrollContainer = mobileDevice ? window : '.scroll-container';
var scrollContent = '.content-container';		
var destY = $(scrollContainer).scrollTop();
var currentY = $(scrollContainer).scrollTop();
var lastScrollTop = $(scrollContainer).scrollTop();
var smoothScrolling = false;
var dragScrolling = false;
var scrollbarDragging = false;
var scrollbarDraggingOffset = 0;

var smoothScrollMove = function()
{
	var $scrollContainer = $(scrollContainer);
	if (smoothScrolling)
	{
	    if (Math.round(currentY) != destY && lastScrollTop == $scrollContainer.scrollTop())
	    {
	    	var d = (destY - currentY) * 0.13;
	    	currentY += d;
	        $scrollContainer.scrollTop(currentY);
	        lastScrollTop = $scrollContainer.scrollTop();
	        $(window).trigger('smooth-scroll', [currentY]);
	    }
	    else
		{
			smoothScrolling = false;
			dragScrolling = false;
			currentY = $scrollContainer.scrollTop();
			$(window).trigger('smooth-scroll-stop', [currentY]);
		}
	}
	else currentY = destY = $scrollContainer.scrollTop();
    
    window.requestAnimationFrame(smoothScrollMove);
};

function smoothScrollTo(newDestY)
{
	window.scrolled = true;
	$(scrollContainer).stop();
	newDestY = Math.max(newDestY, 0);
	newDestY = Math.min(newDestY, $(scrollContent).height() - $(window).height());
	destY = Math.round(newDestY);
	smoothScrolling = true;
	dragScrolling = scrollbarDragging;
	lastScrollTop = $(scrollContainer).scrollTop();
}

$(window).on('keydown', function(e)
{
	if ($(e.target).is('input, select, textarea')) return;
	switch (e.which)
	{
    	case 36: smoothScrollTo(0); break;
		case 35: smoothScrollTo(999999999); break;
    	case 34: smoothScrollTo(destY + Math.max(50, $(window).height() - 100)); break;
    	case 33: smoothScrollTo(destY - Math.max(50, $(window).height() - 100)); break;
    	case 0:
    	case 32: smoothScrollTo(destY + Math.max(50, $(window).height() - 100) * (e.shiftKey ? -1 : 1)); break;
    	case 40: smoothScrollTo(destY + 20); break;
    	case 38: smoothScrollTo(destY - 20); break;
    	default: return true;
    }
    return false;
});
$(window).on('mousewheel', function(event)
{
	if (event.ctrlKey) return true;
	smoothScrollTo(destY - event.deltaY * event.deltaFactor * 1.3 * (isFirefox ? 2 : 1));
	return false;
});

function initSmoothScroll()
{
	window.requestAnimationFrame(smoothScrollMove);

	$(scrollContainer).on('touchmove', function()
	{
		window.touchMoved = true;
	});
	$(scrollContainer).on('scroll', function()
	{
		if (mobileDevice && window.touchMoved) window.scrolled = true;
		//if (isIEorEdge && !mobileDevice) $(scrollContent).css('transform', "translateY(-"+$(scrollContainer).scrollTop()+"px)");
	});

	if (!mobileDevice) $(scrollContainer).addClass('active');
	/*if (isIEorEdge && !mobileDevice)
	{
		$(scrollContent).css({ position: 'fixed', top: 0, left: 0, right: 0 });
		$(window).resize(function()
		{
			$('.scroll-content-placeholder').height($(scrollContent).height());
		});
	}*/
}

//****************************************************************************************************//
// parallax

function initParallax()
{
	if (!mobileDevice && !isIEorEdge)
	{
		$(window).on('resize', function()
		{
			var navHeight = 0;
			
			var height = $('.slider').outerHeight() - ($('.slider').outerHeight() - ($(window).height() - navHeight)) / 2;
			
			$(scrollContainer).scroll();
		});
		
		$(scrollContainer).on('scroll', function()
		{
			var scrollY = $(scrollContainer).scrollTop();
			var contentOffset = $(scrollContent).offset().top;
			var navHeight = 0;
			
			$('.scroll-to-top').toggleClass('visible', scrollY > $(window).height() / 2);
			
			if ($('.slider').length)
			{
				var newY = (scrollY + navHeight - $('.slider').offset().top + contentOffset) / 2;
				$('.slide-img-container').css('transform', "translateY("+newY+"px)");
				$('.slide-inner').css('transform', "translateY("+(newY/2)+"px)");
			}
		});
	}
}

//****************************************************************************************************//
// arise

function arise($element)
{
	var data = $element[0].dataset;
	
	if (data.ariseJs == '')
	{
		setTimeout(function() {
			window[data.arise ? data.arise : data.ariseManual]($element, data.ariseDuration || 1000);
		}, data.ariseDelay || 0);
	}
	else
	{
		$element.css('opacity', 1);
		if (data.ariseDuration)
			$element.css('animation-duration', data.ariseDuration)
			        .css('-webkit-animation-duration', data.ariseDuration);
		if (data.ariseDelay)
			$element.css('animation-delay', data.ariseDelay)
			        .css('-webkit-animation-delay', data.ariseDelay);
		$element.addClass('animated '+(data.arise ? data.arise : data.ariseManual));
	}
	data.arised = true;
}

function ariseInViewport()
{
    var scrollTop = $(scrollContainer).scrollTop();
    var viewportHeight = $(window).height();
    var scrollOffset = $(scrollContent).offset().top;
    var contentHeight = $(scrollContent).height();
    var navHeight = 50;

    $('[data-arise], [data-arise-trigger]').not('[data-arised=true]').each(function()
    {
    	var offset = Math.min(parseInt(this.dataset.ariseOffset) || 100, viewportHeight / 3); //$(this).data('arise-offset')
    	var offsetBottom = Math.min(parseInt(this.dataset.ariseOffsetBottom) || 100, viewportHeight / 3);
        var top = $(this).offset().top - scrollOffset;
		var height = $(this).outerHeight();
		//console.log(`${offset} ${offsetBottom} ${top} ${height} ${scrollTop} ${viewportHeight} ${scrollOffset} ${contentHeight} ${scrollTop + viewportHeight >= Math.min(top + offset, contentHeight)} ${scrollTop + navHeight <= Math.max(top + height - offsetBottom, navHeight)}`);
        if(scrollTop + viewportHeight >= Math.min(top + offset, contentHeight) && scrollTop + navHeight <= Math.max(top + height - offsetBottom, navHeight))
        {
        	if (!this.dataset.arised)
        	{
        		if (this.dataset.ariseTrigger)
        		{
        			$('[data-arise-manual][data-arise-group="'+this.dataset.ariseTrigger+'"]')
        				.each(function(i, e) { arise($(e)); });
        			this.dataset.arised = true;
    			}
        		else if (this.dataset.ariseGroup)
        			$('[data-arise][data-arise-group="'+this.dataset.ariseGroup+'"]')
        				.each(function(i, e) { arise($(e)); });
        		else
        			arise($(this));
            }
        }
    });
}

window.ariseInViewport = ariseInViewport;

//****************************************************************************************************//
// scrollbar

function initScrollBar()
{
	if (!mobileDevice)
	{
		$(window).resize(function()
		{
			$('.scrollbar-thumb').outerHeight(Math.max($(window).height() * $(window).height() / $(scrollContent).outerHeight(), 40));
			
			//$('.slider').trigger('refresh.owl.carousel');
		});
		$(scrollContainer).scroll(function()
		{
			var scrollY = scrollbarDragging || dragScrolling ? destY : $(scrollContainer).scrollTop();
			var scrollbarY = (scrollY / ($(scrollContent).outerHeight() - $(window).height())) * ($(window).height() - $('.scrollbar-thumb').outerHeight());
			//$('.scrollbar-thumb').velocity({translateY: scrollbarY}, 0);
			$('.scrollbar-thumb').css('transform', 'translateY('+scrollbarY+'px)');
		});
		$('.scrollbar-thumb').on('mousedown', function(event)
		{
			$('.scrollbar-thumb').addClass('active');
			scrollbarDragging = true;
			scrollbarDraggingOffset = event.clientY - ($('.scrollbar-thumb').offset().top - $('.scrollbar-track').offset().top);
			return false;
		});
		$(window).on('mouseup', function()
		{
			$('.scrollbar-thumb').removeClass('active');
			scrollbarDragging = false;
			$(window).trigger('scrollbar-drag-stop');
		});
		$(window).on('mousemove', function(event)
		{
			if (scrollbarDragging)
			{
				if (event.buttons != 1)
				{
					$('.scrollbar-thumb').removeClass('active');
					scrollbarDragging = false;
				}
				else
				{
					var thumbTop = event.clientY - scrollbarDraggingOffset;
					var thumbMax = $(window).height() - $('.scrollbar-thumb').outerHeight();
					var percent = Math.max(0, Math.min(1, thumbTop / thumbMax));
					var max = $(scrollContent).outerHeight() - $(window).height();
					smoothScrollTo(percent * max);
				}
				return false;
			}
		});
		
		var timer;
		window.scrolling = false;
		
		$(scrollContainer).on('scroll', function()
		{
			if (lastScrollTop == currentY) return;
			if (!window.scrolling)
			{
				window.scrolling = true;
				$(window).trigger('scroll-start');
			}
			clearTimeout(timer);
			timer = setTimeout(function()
			{
				window.scrolling = false;
				$(window).trigger('scroll-stop');
			}, 150);
		});
		
		var mouseNearScrollbar = false;
		
		$(window).on('mousemove', function(event)
		{
			if (mouseNearScrollbar != (event.clientX >= $(window).width() - 50))
			{
				mouseNearScrollbar = event.clientX >= $(window).width() - 50;
				$(window).trigger('mouse-near-scrollbar');
			}
		});
		
		$(window).on('mouse-near-scrollbar scroll-start scroll-stop scrollbar-drag-stop', function(event)
		{
			$('.scrollbar-track').toggleClass('active', window.scrolling || scrollbarDragging || mouseNearScrollbar);
		});

		if (!isIEorEdge)
		{
			$(scrollContainer).css('overflow', 'scroll');
			$(scrollContainer).css('overflow-x', 'hidden');
			$(scrollContainer).css('right', $(scrollContent).width() - $(scrollContainer).width());
		}
	}
}

/****************************************************************************************************/
// płynne przełączanie stron

function openPage()
{
	$('.curtain').removeClass('active');
	var duration = 600;
	$('.navbar').velocity({ translateY: [0, -50] }, duration, 'easeOutCubic');
	setTimeout(() => window.resetBookAnimation(), 600);
}

function closePage()
{
	window.resetBookAnimation();
	setTimeout(() => window.startBookAnimation(), 1000);
	$('.curtain').addClass('active');
}

$(window).on('smooth-scroll-stop', function()
{
	history.changeState({ scrollTop: $(scrollContainer).scrollTop() });
});

$('body').on('click', 'a:not(.ajax):not(.zoom):not(.lightbox)', function(e)
{
	if (e.ctrlKey) return;
	
	var target = $(this).attr('target') == undefined || $(this).attr('target') == '_self';
	//var isHttp = $(this).prop("protocol").indexOf("http") === 0;
	var base = $('base').attr('href');

	if (this.href == window.location.href) history.changeState({ postCount: null });

	if (target && (this.href.startsWith(base) || this.hostname == 'www.niekulturalnie.pl'))
	{
		history.changeState({ scrollTop: $(scrollContainer).scrollTop() });
		var url = this.href.replace(base, '').replace(this.protocol+'//'+this.hostname, '');
		router.push((url.startsWith('/') ? '' : '/') + url);
	    return false;
	}
});

$(window).on('unload', function()
{
	history.changeState({ scrollTop: $(scrollContainer).scrollTop() });
	//window.localStorage.setItem('cache', cache);
});

/****************************************************************************************************/
// menu mobilne (pełnoekranowe)

function lockForAnimation(selector, duration)
{
	if ($(selector).data('animating')) return true;
	$(selector).data('animating', 1);
	setTimeout(function() {
		$(selector).data('animating', 0);
	}, duration);
	return false;
}

function showFullscreenMenu()
{
	if (lockForAnimation('.menu-fullscreen', 600)) return;
	$('.menu-fullscreen').show();
	$('.menu-fullscreen').velocity({ translateX: [0, '100%'], translateZ: 0 }, 600, 'easeInOutCubic');
	$('.menu-fullscreen-content').velocity({ translateX: [0, '-100%'], translateZ: 0 }, 600, 'easeInOutCubic');
	$('.menu-fullscreen-logo')
			.velocity({ opacity: 0, translateX: 100 }, 0)
			.delay(400)
			.velocity({ opacity: 1, translateX: 0, translateZ: 0 }, 400, 'ease');
	var delay = 500;
	$('.menu-fullscreen-nav > li').each(function() {
		$(this)
			.velocity({ opacity: 0, translateX: 50 }, 0)
			.delay(delay)
			.velocity({ opacity: 1, translateX: 0, translateZ: 0 }, 400, 'ease');
		delay += 50;
	});
	if (mobileDevice) $('body').css('overflow', 'hidden');
}

function hideFullscreenMenu(callback = null)
{
	if (!$('.menu-fullscreen').is(':visible') || lockForAnimation('.menu-fullscreen', 400)) return;
	$('.menu-fullscreen').velocity({ translateX: ['100%', 0], translateZ: 0 }, 400, 'easeInOutCubic')
	.queue(function(next) {
		//$('.menu-fullscreen').hide();
		if (callback) callback();
		next();
	});
	$('.menu-fullscreen-content').velocity({ translateX: ['-100%', 0], translateZ: 0 }, 400, 'easeInOutCubic');
	if (mobileDevice) $('body').css('overflow', 'auto');
}

window.showFullscreenMenu = showFullscreenMenu;
window.hideFullscreenMenu = hideFullscreenMenu;

//****************************************************************************************************//

function fixMobileLinks()
{
	$("a").each(function()
	{
		if (this.hostname.indexOf(domain) != -1)
		{
			var searchParams = new URLSearchParams(this.search);
			if (searchParams.get('m') != 1)
			{
				searchParams.set('m', 1);
				this.search = searchParams.toString();
			}
		}
	});
}

function init(element = 'body', force = false)
{
	if (!force && !window.loaded) return;
	var $element = $(element);
	$element.find('[data-toggle="tooltip"]').tooltip();
	$element.find('[data-toggle="tooltip-fixed"]').tooltip({ container: '.tooltip-fixed' });
	$element.find('a').filter(function() { return this.href.match(/\.png$|\.gif$|\.jpg$/i); }).addClass('lightbox');
	$element.find('.lightbox').magnificPopup({ type: 'image', image: { titleSrc: 'data-title' }, gallery: { enabled:true, tCounter: '%curr% / %total%' } });
	$element.find('[data-arise], [data-arise-manual]').not('.arise-visible').not('[data-arise-js]').not('[data-arised]').css('opacity', 0);
	if (mobileDevice && env != 'dev') fixMobileLinks();
	objectFitImages($('body').find('img'));
	if (router.app && router.app.$route.name == 'home') initSlider();
	insertBloggerSections();
	if (router.app && ['page', 'post'].includes(router.app.$route.name))
	{
		setTimeout(function()
		{
			if (typeof FB !== 'undefined') FB.XFBML.parse($element[0]);
			$('#bb-script').remove();
			$('body').append('<script id="bb-script" src="https://buybox.click/js/bb-loader.min.js"></script>');
		}, 1000);
	}
	restoreScrollTop();
	if (window.ariseInitalized) ariseInViewport();
}
window.init = init;

function initStart()
{
	init('body', true);
	initForm();
	initFollowGooglePlus();
	initSmoothScroll();
	initParallax();
	initScrollBar();
	$(scrollContainer).scroll(function()
	{
		if ($('.infinite-more').length && $(scrollContainer).scrollTop() + $(window).height() >= $('.infinite-more').offset().top - $(scrollContent).offset().top)
			$('.infinite-more').trigger('click');
	});
	$('.scroll-to-top').click(function()
	{
		//$('html, body').animate({ scrollTop: 0 }, 1000, 'easeOutQuart');
		$(scrollContainer).animate({ scrollTop: 0 }, 1000, 'easeOutQuart');
		$(this).tooltip('hide');
	});
	if (!mobileDevice)
	{
		$('.navbar-search-box, .menu-fullscreen').on('mousewheel', function(event)
		{
			$(this).scrollTop($(this).scrollTop() - event.deltaY * event.deltaFactor * 1.0 * (isFirefox ? 2 : 1));
			if ($(this).prop('scrollHeight') > $(this).height()) return false;
		});
	}
	if (typeof followersIframeOpen == 'function') followersIframeOpen($('.widget.Followers iframe').attr('src'));
}

function initForm()
{
	$('form[name="contact-form-custom"]').submit(function()
	{
		var form = this;
		var errorMessage = 'Wystąpił błąd, sprawdź czy formularz został wypełniony poprawnie.';
		var successMessage = 'Wiadomość została wysłana.';
		var data = $(this).serializeArray().reduce(function(obj, item) {
			obj[item.name] = item.value;
			return obj;
		}, {});
		$.post('https://www.blogger.com/contact-form.do',
		{
			blogID: env == 'dev' ? '5645147778766638791' : blogId,
			name: data.name,
			email: data.email,
			message: data.message,
		})
		.done(function(data)
		{
			data = JSON.parse(data);
			if (data && data.details && data.details.emailSentStatus == 'true')
			{
				$(form).find('.alerts').html($('<div class="alert alert-success"/>').text(successMessage));
				$(form).find(".form-control").val("");
				return;
			}
			$(form).find('.alerts').html($('<div class="alert alert-danger"/>').text(errorMessage));
		})
		.fail(function()
		{
			$(form).find('.alerts').html($('<div class="alert alert-danger"/>').text(errorMessage));
		});
		return false;
	});
}

function initFollowGooglePlus()
{
	var div = `<div class="g-follow" data-annotation="bubble" data-height="24" data-href="https://plus.google.com/117898348171969363368" data-rel="author"></div>`;
	$('.follow-google-plus').append(div);
	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/platform.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
}

function initDelayed()
{
	$(window).trigger('init-delayed');
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.10";
		js.async = true;
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	window.opened = true;
}

// usuwa problem z pojawianiem się outline podczas klikania w przycisk/link
var mouseFocus = false;
$("body").on("mousedown", function(e)
{
	mouseFocus = true;
	setTimeout(() => mouseFocus = false, 1);
});
$("body").on("focus", '*:not(.form-control)', function(e)
{
	if (mouseFocus)
        $(this).css("outline", "none").on("blur", function()
        {
            $(this).off("blur").css("outline", "");
        });
});

$(window).resize(function()
{
	$('.slide').css('max-height', $(window).height());
	$('.navbar-search-box').css('max-height', $(window).height() - 50);
	$('.main-content').css('min-height', $(window).height() - $('footer').outerHeight());
});

//$('.bb-widget').attr('data-bb-skip_jQuery', 1);

$.fn.isInViewport = function()
{
	var el = this[0];
	var rect = el.getBoundingClientRect();

    var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
    var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

    var vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
    var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

    return (vertInView && horInView);
};

$.expr[':'].inview = function(elem, index, match)
{
	return $(elem).isInViewport();
};

window.loaded = false;

function pageLoaded()
{
	if (window.loaded) return;
	setTimeout(function()
	{
		initStart();
		window.loaded = true;
		setTimeout(function()
		{
			//$('img[data-src]:visible:inview').each((i, e)=>$(e).attr('src', $(e).data('src')));
			//$('img:visible:inview').imagesLoaded(function()
			//{
				//$('img[data-src]').each((i, e)=>$(e).attr('src', $(e).data('src')));
				new ResizeSensor($('.content-container'), function() { $(window).resize(); restoreScrollTop(); });
				$(window).resize();
				restoreScrollTop();
				if (window.swiper) window.swiper.update();
				//$('.slider').trigger('refresh.owl.carousel').trigger('stop.owl.autoplay');
				$(scrollContainer).scroll(ariseInViewport);
				ariseInViewport();
				window.ariseInitalized = true;
				openPage();
				setTimeout(() => initDelayed(), 800);
			//});
		});
	});
}