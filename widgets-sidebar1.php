<?php if ($env == 'dev'): ?>
	
<div class='widget Image' data-version='1' id='Image1'>
	<h2>O mnie</h2>
	<div class='widget-content'>
		<img alt='O mnie' height='1067' id='Image1_img' src='http://4.bp.blogspot.com/-ZM5RF42HNCY/Wglsc28RO-I/AAAAAAAAsbI/e1DDM-tn6NkuGxQdICAocNrtFH_LWZ4qACK4BGAYYCw/s1600/photo-1422513391413-ddd4f2ce3340.jpg' width='1600'/>
		<br/>
		<span class='caption'>Dziewczę o wielce egzotycznym imieniu Kasia. Czyta, rysuje, próbuje gotować. W wolnych chwilach szuka miejsca i pieniędzy na kolejne książki, je rodzynki i biega za kaczkami.</span>
	</div>
	<div class='clear'></div>
</div>
<div class='widget Stats' data-version='1' id='Stats1'>
	<h2>Łączna liczba wyświetleń</h2>
	<div class='widget-content'>
		<div id='Stats1_content' style='display: block;'>
			<span class='counter-wrapper text-counter-wrapper' id='Stats1_totalCount'>363,542</span>
			<div class='clear'></div>
		</div>
	</div>
</div>
<div class='widget BlogArchive' data-version='1' id='BlogArchive1'>
	<h2>Archiwum bloga</h2>
	<div class='widget-content'>
		<div id='ArchiveList'>
			<div id='BlogArchive1_ArchiveList'>
				<ul class='hierarchy'>
					<li class='archivedate expanded'>
						<a class='toggle' href='javascript:void(0)'> <span class='zippy toggle-open'> &#9660;&#160; </span> </a>
						<a class='post-count-link' href='http://niekulturalnie-test.blogspot.com/2017/'> 2017 </a>
						<span class='post-count' dir='ltr'>(2)</span>
						<ul class='hierarchy'>
							<li class='archivedate expanded'>
								<a class='toggle' href='javascript:void(0)'> <span class='zippy toggle-open'> &#9660;&#160; </span> </a>
								<a class='post-count-link' href='http://niekulturalnie-test.blogspot.com/2017/09/'> września </a>
								<span class='post-count' dir='ltr'>(1)</span>
								<ul class='posts'>
									<li>
										<a href='http://niekulturalnie-test.blogspot.com/2017/08/dolor-sit-amet-enim.html'>Dolor sit amet enim</a>
									</li>
								</ul>
							</li>
						</ul>
						<ul class='hierarchy'>
							<li class='archivedate collapsed'>
								<a class='toggle' href='javascript:void(0)'> <span class='zippy'> &#9658;&#160; </span> </a>
								<a class='post-count-link' href='http://niekulturalnie-test.blogspot.com/2017/08/'> sierpnia </a>
								<span class='post-count' dir='ltr'>(1)</span>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div class='clear'></div>
	</div>
</div>

<?php else: ?>

<b:widget id='Image1' locked='false' title='O mnie' type='Image'>
  <b:widget-settings>
    <b:widget-setting name='displayUrl'>http://4.bp.blogspot.com/-teLSwUcnTFY/WWuhugVQkiI/AAAAAAAAP3w/2X2Dh1ftfNwWiFNCJSnrc1hZb-iOORKhQCK4BGAYYCw/s300/16836597_1427936917247866_473502665414068244_o.jpg</b:widget-setting>
    <b:widget-setting name='displayHeight'>225</b:widget-setting>
    <b:widget-setting name='sectionWidth'>200</b:widget-setting>
    <b:widget-setting name='shrinkToFit'>false</b:widget-setting>
    <b:widget-setting name='displayWidth'>300</b:widget-setting>
    <b:widget-setting name='link'/>
    <b:widget-setting name='caption'>Dziewczę o wielce egzotycznym imieniu Kasia. Czyta, rysuje, próbuje gotować. W wolnych chwilach szuka miejsca i pieniędzy na kolejne książki, je rodzynki i biega za kaczkami.</b:widget-setting>
  </b:widget-settings>
  <b:includable id='main'>
    <b:if cond='data:title != &quot;&quot;'>
      <h2><data:title/></h2>
    </b:if>
    <div class='widget-content'>
      <b:tag cond='data:link' expr:href='data:link' name='a'>
        <img expr:alt='data:title' expr:height='data:height' expr:id='data:widget.instanceId + &quot;_img&quot;' expr:src='data:sourceUrl' expr:width='data:width'/>
      </b:tag>
      <br/>
      <b:if cond='data:caption'>
        <span class='caption'><data:caption/></span>
      </b:if>
    </div>
    <b:include name='quickedit'/>
  </b:includable>
</b:widget>
<b:widget id='Stats1' locked='false' title='Łączna liczba wyświetleń' type='Stats'>
  <b:widget-settings>
    <b:widget-setting name='showGraphicalCounter'>false</b:widget-setting>
    <b:widget-setting name='showAnimatedCounter'>false</b:widget-setting>
    <b:widget-setting name='showSparkline'>false</b:widget-setting>
    <b:widget-setting name='sparklineStyle'>BLACK_TRANSPARENT</b:widget-setting>
    <b:widget-setting name='timeRange'>ALL_TIME</b:widget-setting>
  </b:widget-settings>
  <b:includable id='main'>
  <b:if cond='data:title != &quot;&quot;'><h2><data:title/></h2></b:if>
  <div class='widget-content'>
    <!-- Content is going to be visible when data will be fetched from server. -->
    <div expr:id='data:widget.instanceId + &quot;_content&quot;' style='display: none;'>
      <!-- Counter and image will be injected later via AJAX call. -->
      <b:if cond='data:showSparkline'>
        <img alt='Sparkline' expr:id='data:widget.instanceId + &quot;_sparkline&quot;' height='30' width='75'/>
      </b:if>
      <span expr:class='&quot;counter-wrapper &quot; + (data:showGraphicalCounter ? &quot;graph-counter-wrapper&quot; : &quot;text-counter-wrapper&quot;)' expr:id='data:widget.instanceId + &quot;_totalCount&quot;'>
      </span>
      <b:include name='quickedit'/>
    </div>
  </div>
</b:includable>
</b:widget>
<b:widget id='BlogArchive1' locked='false' title='Archiwum bloga' type='BlogArchive'>
  <b:widget-settings>
    <b:widget-setting name='showStyle'>HIERARCHY</b:widget-setting>
    <b:widget-setting name='yearPattern'>yyyy</b:widget-setting>
    <b:widget-setting name='showWeekEnd'>true</b:widget-setting>
    <b:widget-setting name='monthPattern'>MMMM</b:widget-setting>
    <b:widget-setting name='dayPattern'>MMM dd</b:widget-setting>
    <b:widget-setting name='weekPattern'>MM/dd</b:widget-setting>
    <b:widget-setting name='chronological'>false</b:widget-setting>
    <b:widget-setting name='showPosts'>true</b:widget-setting>
    <b:widget-setting name='frequency'>MONTHLY</b:widget-setting>
  </b:widget-settings>
  <b:includable id='main'>
  <b:if cond='data:title != &quot;&quot;'>
    <h2><data:title/></h2>
  </b:if>
  <div class='widget-content'>
  <div id='ArchiveList'>
  <div expr:id='data:widget.instanceId + &quot;_ArchiveList&quot;'>
    <b:include cond='data:style == &quot;HIERARCHY&quot;' data='data' name='interval'/>
    <b:include cond='data:style == &quot;FLAT&quot;' data='data' name='flat'/>
    <b:include cond='data:style == &quot;MENU&quot;' data='data' name='menu'/>
  </div>
  </div>
  <b:include name='quickedit'/>
  </div>
</b:includable>
  <b:includable id='flat' var='data'>
  <ul class='flat'>
    <b:loop values='data:data' var='i'>
      <li class='archivedate'>
        <a expr:href='data:i.url'><data:i.name/></a> (<data:i.post-count/>)
      </li>
    </b:loop>
  </ul>
</b:includable>
  <b:includable id='interval' var='intervalData'>
  <b:loop values='data:intervalData' var='interval'>
    <ul class='hierarchy'>
      <li expr:class='&quot;archivedate &quot; + data:interval.expclass'>
        <b:include cond='data:interval.toggleId' data='interval' name='toggle'/>
        <a class='post-count-link' expr:href='data:interval.url'>
          <data:interval.name/>
        </a>
        <span class='post-count' dir='ltr'>(<data:interval.post-count/>)</span>
        <b:include cond='data:interval.data' data='interval.data' name='interval'/>
        <b:include cond='data:interval.posts' data='interval.posts' name='posts'/>
      </li>
    </ul>
  </b:loop>
</b:includable>
  <b:includable id='menu' var='data'>
  <select expr:id='data:widget.instanceId + &quot;_ArchiveMenu&quot;'>
    <option value=''><data:title/></option>
    <b:loop values='data:data' var='i'>
      <option expr:value='data:i.url'><data:i.name/> (<data:i.post-count/>)</option>
    </b:loop>
  </select>
</b:includable>
  <b:includable id='posts' var='posts'>
  <ul class='posts'>
    <b:loop values='data:posts' var='post'>
      <li><a expr:href='data:post.url'><data:post.title/></a></li>
    </b:loop>
  </ul>
</b:includable>
  <b:includable id='toggle' var='interval'>
  <a class='toggle' href='javascript:void(0)'>
    <span expr:class='&quot;zippy&quot; + (data:interval.expclass == &quot;expanded&quot; ? &quot; toggle-open&quot; : &quot;&quot;)'>
      <b:if cond='data:interval.expclass == &quot;expanded&quot;'>
        &#9660;&#160;
      <b:elseif cond='data:blog.languageDirection == &quot;rtl&quot;'/>
        &#9668;&#160;
      <b:else/>
        &#9658;&#160;
      </b:if>
    </span>
  </a>
</b:includable>
</b:widget>

<?php endif ?>