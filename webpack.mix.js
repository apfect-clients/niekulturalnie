var mix = require('laravel-mix');
var webpack = require('webpack');

mix
.setPublicPath('public')
.setResourceRoot('../')
.js('./js/app.js', 'js')
.js('./js/book.js', 'js')
.js('./js/data-processing-worker.js', 'js')
.sass('./css/app.scss', 'css', {
	//includePaths: ['node_modules/bootstrap-sass/assets/stylesheets']
})
.webpackConfig({
   	resolve: {
		alias: { jquery: "jquery/src/jquery" }
	},
	plugins: [
		new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /pl/)
	],
	module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules\/(?!(dom7|swiper)\/).*|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: Config.babel()
                    }
                ]
            }
        ]
    }
});

//if (!mix.inProduction()) mix.sourceMaps();