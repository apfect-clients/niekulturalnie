<?php
	include "data.php";
	
	$blogger = false;
	$env = $_GET['env'] ?: 'dev'; // 'dev' / 'staging' / 'prod'
	$assetsBase = [
		'dev'=>'public',
		'staging'=>'http://demo.apfect.pl/niekulturalnie',
		'prod'=>'https://cdn.rawgit.com/apfect/niekulturalnie-assets/e3fb7c5f2ed9f99ee57d83fe46427bf77fcb0be3',
	][$env];
	$baseHref = [
		'dev'=>"http://{$_SERVER['HTTP_HOST']}/blogkasi/",
		'staging'=>'http://niekulturalnie-test.blogspot.com/',
		'prod'=>'http://www.niekulturalnie.pl/'
	][$env];
	$view = @$_GET['view'] ?: 'home';
	if ($view == 'label' && @$_GET['label'] == 'recenzja') $view = 'reviews';
?>
<?php if ($env == 'dev'): ?>
	<!DOCTYPE html>
<?php else: ?>
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE html [
	    <!ENTITY nbsp "&#160;"> 
	    <!ENTITY bull "&#8226;"> 
	]>
<?php endif ?>
<html b:css='true' b:b:defaultwidgetversion='2' b:layoutsVersion='3' b:responsive='true' expr:dir='data:blog.languageDirection' xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>
	
<head>
	<base href='<?=$baseHref?>'/>
	
	<?php if ($env == 'dev'): ?>
		<link href='https://www.blogger.com/static/v1/widgets/3213516723-css_bundle_v2.css' rel='stylesheet' type='text/css'/>
	<?php endif ?>
	
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	
	<?php include "_meta.php" ?>
	
	<link rel="stylesheet" type="text/css" href="<?=$assetsBase?>/css/app.css?<?=time()?>"/>
	
	<?php if ($env != 'dev'): ?>
   		<b:include data='blog' name='all-head-content'/>
		<b:skin version='1.1.0'><![CDATA[]]></b:skin>
		<b:template-skin><![CDATA[
			
		  body#layout .navbar { display: none; }
	      body#layout .sidebar-container { float: right; width: 33.3333%; }
	      body#layout .main-container { float: left; width: 66.6666%; }
	      body#layout .footer-column { float: left; width: 33.3333%; }
	      body#layout .footer-columns { clear: both; }
	      
		]]></b:template-skin>
	<?php endif ?>
</head>

<body data-env='<?=$env?>' data-assets='<?=$assetsBase?>'>
	<script>
		if (location.protocol == 'https:')
			location.href = 'http:' + location.href.substring(location.protocol.length);
	</script>

	<div id="fb-root"></div>

	<div id='temp-curtain' style='position: fixed; z-index: 99999; top: 0; left: 0; right: 0; bottom: 0; background: #000000;'></div>
	
	<div class='curtain active'>
		<div class='curtain-content'>
			<?php include "book.php" ?>
		</div>
	</div>

	<div class='blogger-sections'>
		<div class='main-container'>
			<b:section class='main' id='main1' showaddelement='no'>
				<?php include "widgets-main.php" ?>
			</b:section>
		</div>
		<div class='sidebar-container'>
			<b:section class='sidebar section' id='sidebar1' showaddelement='yes'>
				<?php include "widgets-sidebar1.php" ?>
			</b:section>
			<b:section class='sidebar section' id='sidebar2' showaddelement='yes'>
				<?php include "widgets-sidebar2.php" ?>
			</b:section>
		</div>
		<div class='row row-no-gutter row-same-height footer-columns'>
			<div class='col-sm-4 footer-column'>
				<b:section class='footer' id='footer1' showaddelement='yes'>
					<?php include "widgets-footer1.php" ?>
				</b:section>
			</div>
			<div class='col-sm-4 footer-column'>
				<b:section class='footer' id='footer2' showaddelement='yes'>
					<?php include "widgets-footer2.php" ?>
				</b:section>
			</div>
			<div class='col-sm-4 footer-column'>
				<b:section class='footer' id='footer3' showaddelement='yes'>
					<?php include "widgets-footer3.php" ?>
				</b:section>
			</div>
		</div>
	</div>
	
	<div id='app'></div>

	<div class='scrollbar-track'>
		<div class='scrollbar-thumb'></div>
	</div>

	<script type="text/javascript" src="<?=$assetsBase?>/js/app.js?<?=time()?>"></script>

	<b:include data='blog' name='google-analytics'/>
</body>

</html>