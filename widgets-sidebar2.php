<?php if ($env == 'dev'): ?>
	
<div class='widget Image' data-version='1' id='Image2'>
	<h2>Teraz czytam</h2>
	<div class='widget-content'>
		<img alt='Teraz czytam' id='Image2_img' src='https://www.swiatksiazki.pl/media/catalog/product/cache/image/700x700/e9c3970ab036de70892d86c6d221abfe/9/9/99906358334.jpg' />
		<br/>
	</div>
	<div class='clear'></div>
</div>
<div class='widget Image' data-version='1' id='Image3'>
	<h2>Snapchat</h2>
	<div class='widget-content'>
		<img alt='Snapchat' height='699' id='Image3_img' src='http://2.bp.blogspot.com/-K8UCjiuQ9_8/WT7P96L2gRI/AAAAAAAAPbc/ZXm1_WPLd6sPphVinUzxEB3zqVXtUL-iACK4B/s1600/snap.jpg' width='782'/>
		<br/>
	</div>
	<div class='clear'></div>
</div>

<?php else: ?>

<b:widget id='Image2' locked='false' title='Teraz czytam' type='Image'>
  <b:widget-settings>
    <b:widget-setting name='displayUrl'>http://bookgeek.pl/wp-content/uploads/2017/07/wladca-cieni-1.jpg</b:widget-setting>
    <b:widget-setting name='displayHeight'>898</b:widget-setting>
    <b:widget-setting name='sectionWidth'>200</b:widget-setting>
    <b:widget-setting name='shrinkToFit'>false</b:widget-setting>
    <b:widget-setting name='displayWidth'>600</b:widget-setting>
    <b:widget-setting name='link'/>
    <b:widget-setting name='caption'/>
  </b:widget-settings>
  <b:includable id='main'>
    <b:if cond='data:title != &quot;&quot;'>
      <h2><data:title/></h2>
    </b:if>
    <div class='widget-content'>
      <b:tag cond='data:link' expr:href='data:link' name='a'>
        <img expr:alt='data:title' expr:height='data:height' expr:id='data:widget.instanceId + &quot;_img&quot;' expr:src='data:sourceUrl' expr:width='data:width'/>
      </b:tag>
      <br/>
      <b:if cond='data:caption'>
        <span class='caption'><data:caption/></span>
      </b:if>
    </div>
    <b:include name='quickedit'/>
  </b:includable>
</b:widget>
<b:widget id='Image3' locked='false' title='Snapchat' type='Image'>
  <b:widget-settings>
    <b:widget-setting name='displayUrl'>http://2.bp.blogspot.com/-K8UCjiuQ9_8/WT7P96L2gRI/AAAAAAAAPbc/ZXm1_WPLd6sPphVinUzxEB3zqVXtUL-iACK4B/s1600/snap.jpg</b:widget-setting>
    <b:widget-setting name='displayHeight'>699</b:widget-setting>
    <b:widget-setting name='sectionWidth'>200</b:widget-setting>
    <b:widget-setting name='shrinkToFit'>false</b:widget-setting>
    <b:widget-setting name='displayWidth'>782</b:widget-setting>
    <b:widget-setting name='link'/>
    <b:widget-setting name='caption'/>
  </b:widget-settings>
  <b:includable id='main'>
    <b:if cond='data:title != &quot;&quot;'>
      <h2><data:title/></h2>
    </b:if>
    <div class='widget-content'>
      <b:tag cond='data:link' expr:href='data:link' name='a'>
        <img expr:alt='data:title' expr:height='data:height' expr:id='data:widget.instanceId + &quot;_img&quot;' expr:src='data:sourceUrl' expr:width='data:width'/>
      </b:tag>
      <br/>
      <b:if cond='data:caption'>
        <span class='caption'><data:caption/></span>
      </b:if>
    </div>
    <b:include name='quickedit'/>
  </b:includable>
</b:widget>

<?php endif ?>