<?php if ($env == 'dev'): ?>
	
<?php else: ?>

<b:widget id='Followers1' locked='false' title='Obserwatorzy' type='Followers'>
  <b:widget-settings>
    <b:widget-setting name='borderColorTransparent'>true</b:widget-setting>
    <b:widget-setting name='useTemplateDefaultStyles'>false</b:widget-setting>
    <b:widget-setting name='contentSecondaryTextColor'>#aaaaaa</b:widget-setting>
    <b:widget-setting name='contentHeadlineColor'>#aaaaaa</b:widget-setting>
    <b:widget-setting name='endcapTextColor'>#aaaaaa</b:widget-setting>
    <b:widget-setting name='contentTextColor'>#aaaaaa</b:widget-setting>
    <b:widget-setting name='contentSecondaryLinkColor'>#ffffff</b:widget-setting>
    <b:widget-setting name='endcapLinkColor'>#aaaaaa</b:widget-setting>
    <b:widget-setting name='contentLinkColor'>#aaaaaa</b:widget-setting>
  </b:widget-settings>
  <b:includable id='main'>
  <b:if cond='data:title != &quot;&quot; and data:codeSnippet != &quot;&quot;'>
    <h2 class='title'><data:title/></h2>
  </b:if>
  <div class='widget-content'>
    <div expr:id='data:widget.instanceId + &quot;-wrapper&quot;'>
      <b:if cond='data:codeSnippet != &quot;&quot;'>
        <div>
          <data:codeSnippet/>
        </div>
      </b:if>
    </div>
    <b:include name='quickedit'/>
  </div>
</b:includable>
</b:widget>


<b:widget id='FollowByEmail1' locked='false' title='Obserwuj przez e-mail' type='FollowByEmail' version='1'>
  <b:includable id='main'>
  <b:if cond='data:title != &quot;&quot;'><h2 class='title'><data:title/></h2></b:if>
  <div class='widget-content'>
    <div class=''>
      <form action='https://feedburner.google.com/fb/a/mailverify' expr:onsubmit='&quot;window.open(\&quot;https://feedburner.google.com/fb/a/mailverify?uri=&quot; + data:feedPath + &quot;\&quot;, \&quot;popupwindow\&quot;, \&quot;scrollbars=yes,width=550,height=520\&quot;); return true&quot;' method='post' target='popupwindow'>
        
        <div class='input-group'>
	      <input class='form-control' name='email' placeholder='Twój adres-email' type='text'/>
	      <span class='input-group-btn'>
	        <button class='btn btn-default' type='button'>Obserwuj</button>
	      </span>
	    </div>
        <input expr:value='data:feedPath' name='uri' type='hidden'/>
        <input name='loc' type='hidden' value='en_US'/>
      </form>
    </div>
  </div>
  <span class='item-control blog-admin'>
    <b:include name='quickedit'/>
  </span>
</b:includable>
</b:widget>




<?php endif ?>