<?php if ($env != 'dev'): ?>
    <b:if cond='data:view.isHomepage'>
        <title><data:blog.title/></title>
        <meta property='og:type' content='website'/>
    </b:if>
    <b:if cond='data:view.isPost'>
        <title><data:view.title/> | <data:blog.title/></title>
        <meta property='og:type' content='article'/>
    </b:if>
    <b:if cond='data:view.isPage'>
        <title><data:view.title/> | <data:blog.title/></title>
        <meta property='og:type' content='article'/>
    </b:if>
    <b:if cond='data:view.isSearch and !data:view.isLabelSearch'>
        <title>Wyniki wyszukiwania dla frazy "<data:view.search.query/>" | <data:blog.title/></title>
        <meta property='og:type' content='website'/>
    </b:if>
    <b:if cond='data:view.isLabelSearch'>
        <title>Lista postów z etykietą "<data:view.search.label/>" | <data:blog.title/></title> 
        <meta property='og:type' content='website'/>
    </b:if>
    <b:if cond='data:view.archive'>
        <title><data:view.archive.rangeMessage/> | Archiwum | <data:blog.title/></title> 
        <meta property='og:type' content='website'/>
    </b:if>
    <b:if cond='data:view.isError'>
        <title>Błąd 404 | <data:blog.title/></title>
        <meta property='og:type' content='website'/>
    </b:if>
<?php endif ?>

<meta property='fb:app_id' expr:content='154070961981095'/>
<meta property='og:image' expr:content='data:view.featuredImage'/>
<meta name='description' expr:content='data:view.description'/>

<link rel="apple-touch-icon" sizes="180x180" href="<?=$assetsBase?>/icons/apple-touch-icon.png"/>
<link rel="icon" type="image/png" sizes="32x32" href="<?=$assetsBase?>/icons/favicon-32x32.png"/>
<link rel="icon" type="image/png" sizes="16x16" href="<?=$assetsBase?>/icons/favicon-16x16.png"/>
<link rel="manifest" href="<?=$assetsBase?>/icons/manifest.json"/>
<link rel="mask-icon" href="<?=$assetsBase?>/icons/safari-pinned-tab.svg" color="#5bbad5"/>
<link rel="shortcut icon" href="<?=$assetsBase?>/icons/favicon.ico"/>
<meta name="msapplication-config" content="<?=$assetsBase?>/icons/browserconfig.xml"/>
<meta name="theme-color" content="#000000"/>